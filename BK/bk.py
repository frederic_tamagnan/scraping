# -*- coding: utf-8 -*-
import json
import urllib2
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import collections

from airflow.hooks.postgres_hook import PostgresHook

def convert(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data







pg_hook= PostgresHook('postgres_ds')
url = 'https://api.burgerking.fr/shops' # --2.0 -H \'Host: api.burgerking.fr\' -H \'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0\' -H \'Accept: application/json, text/javascript, */*; q=0.01\' -H \'Accept-Language: en-US,en;q=0.5\' --compressed -H \'Referer: https://www.burgerking.fr/restaurants\' -H \'Origin: https://www.burgerking.fr\' -H \'Connection: keep-alive\' -H \'If-None-Match: W/"anCEv5IDd11D11K52pcNAg=="'
response = urllib2.urlopen(url)
data = response.read()
values = json.loads(data)

print values


nameTable='frederic.bk'

queryDrop = 'DROP TABLE IF EXISTS '+nameTable
pg_hook.run(queryDrop)

queryCreate = "CREATE TABLE " +nameTable + "(lat double precision, lon double precision,name text,address text,city text,postalCode int, tel text,openingHours json);"
pg_hook.run(queryCreate)

print "LONGUEUR",len(values)
i=0
for shop in values:
    i+=1
    lat=str(float(shop['position']['lat']))

    lon=str(float(shop['position']['lng']))

    zipCode=shop['zipCode'].decode('utf-8')
    phone=shop['phone'].decode('utf-8')
    address=shop['address'].decode('utf-8')
    city=shop['city'].decode('utf-8')
    name=shop['name'].decode('utf-8')
    openingHours=str(convert(shop["schedule"]))
    print openingHours

    queryInsert = 'INSERT INTO ' + nameTable + ' VALUES (' + lat + ',' + lon + ',\'' +name.replace('\'','\'\'')+'\',\'' +address.replace('\'','\'\'') + '\',\'' + city.replace('\'','\'\'') + '\',' +zipCode + ',\'' + phone + '\',\''+openingHours.replace('\'','\"') +'\');'
    print queryInsert
    pg_hook.run(queryInsert)





