# -*- coding: utf-8 -*-


dayJour= {"Lundi":"monday","Mardi":"tuesday","Mercredi":"wednesday","Jeudi":"thursday"
    ,"Vendredi":"friday","Samedi" :"saturday",
    "Simanche":"sunday"}


import scrapy
from xml.etree.ElementTree import XML, fromstring, tostring

from besson.items import POI
import unicodedata

class spiderBesson(scrapy.Spider):
    name = 'spiderBesson'

    start_urls = ['http://www.besson-chaussures.com/files/File/magazines_xml/locations.xml?origLat=48.8665157&origLng=2.3340766&origAddress=34+Rue+Saint-Roch%2C+75001+Paris%2C+France']

    def parse(self, response):
        openingHours={}
        dayJour = {"Lundi": "monday", "Mardi": "tuesday", "Mercredi": "wednesday", "Jeudi": "thursday"
            , "Vendredi": "friday", "Samedi": "saturday",
                   "Dimanche": "sunday"}


        for brickset in response.xpath('/markers/marker'):
            lat=brickset.xpath('@lat').extract_first()
            lon=brickset.xpath('@lng').extract_first()
            name = brickset.xpath('@name').extract_first()
            completeAddress=brickset.xpath('@address').extract_first()
            i=0
            while not(completeAddress[i:i+5].isdigit()) and i<100:
                i+=1
            zipCode=completeAddress[i:i+5]
            city=completeAddress[i+5:]
            address = completeAddress[:i]
            tel=brickset.xpath('@phone').extract_first()
            openingHours={}

            yield POI(address=address,lat=lat,lon=lon,city=city,zipCode=zipCode,tel=tel,openingHours=openingHours,name=name)

