# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from airflow.hooks.postgres_hook import PostgresHook
class MinelliPipeline(object):


    def __init__(self):


        self.pg_hook=PostgresHook('postgres_ds')
        self.nameTable='frederic.minelli'


    def process_item(self, item, spider):

        name=item['name']
        lat=item['lat']
        lon=item['lon']
        adress=item['address']
        city=item['city']
        zipCode=item['zipCode']
        tel=item['tel']
        openingHours=item['openingHours']
        # for key,value in openingHours.items():
        #     for key1,value1 in openingHours[key].items():
        #         openingHours[key][key1]=str(value1)

        print(str(openingHours))
        queryInsert= 'INSERT INTO ' + self.nameTable + ' VALUES (\''+name.replace('\'',' ')+'\','+lat+','+lon+',\''+adress.replace('\'',' ')+'\',\''+city.replace('\'',' ')+'\','+zipCode+',\''+tel+'\',\''+str(openingHours).replace('\'','\"')+'\');'
        self.pg_hook.run(queryInsert)
        return item
    def open_spider(self,spider):

        queryDrop = 'DROP TABLE IF EXISTS '+self.nameTable
        self.pg_hook.run(queryDrop)
        queryCreate = "CREATE TABLE " + self.nameTable + "(name text,lat double precision, lon double precision,address text,city text,zipCode int, tel text,openingHours json);"
        self.pg_hook.run(queryCreate)


