# -*- coding: utf-8 -*-


dayJour= {"Lundi":"monday","Mardi":"tuesday","Mercredi":"wednesday","Jeudi":"thursday"
    ,"Vendredi":"friday","Samedi" :"saturday",
    "Simanche":"sunday"}


import scrapy
from xml.etree.ElementTree import XML, fromstring, tostring
# from utils.subway.items import POI
from minelli.items import POI
import unicodedata

class spiderMinelli(scrapy.Spider):
    name = 'spiderMinelli'

    start_urls = ['https://www.minelli.fr/cookietest/storelocator/store/locations/country_code/FRA']

    def parse(self, response):
        openingHours={}
        dayJour = {"Lundi": "monday", "Mardi": "tuesday", "Mercredi": "wednesday", "Jeudi": "thursday"
            , "Vendredi": "friday", "Samedi": "saturday",
                   "Dimanche": "sunday"}


        for brickset in response.xpath('/root/markers/marker'):
            lat=brickset.xpath('@lat').extract_first()
            lon=brickset.xpath('@lng').extract_first()
            c=brickset.xpath('description')[0].extract()
            strxml=unicodedata.normalize('NFKD', c).encode('ascii', 'ignore').replace('&lt;','<').replace('&gt;','>').replace('&amp;','&')

            xml = fromstring(strxml)
            name=xml.findtext('div[@class="storeinfowindow"]/div[1]')
            zipCodecity=xml.findtext('div[@class="storeinfowindow"]/div[3]') #[@class="storeaddress"]
            address=xml.findtext('div[@class="storeinfowindow"]/div[2]')
            city=zipCodecity[zipCodecity.find(' ')+1:]
            zipCode=zipCodecity[:5]
            tel=xml.findtext('div[@class="storeinfowindow"]/div[4]')

            for i in range(1,8):
                jour=xml.findtext('div[@class="storeinfowindow"]/div['+str(i+5)+']/span[1]')
                day=dayJour[jour]
                openingHours[day]={}
                openClose=xml.findtext('div[@class="storeinfowindow"]/div['+str(i+5)+']/span[3]')
                if len(openClose)==5:
                    openingHours[day]['open']='/'
                    openingHours[day]['close']='/'
                else :
                    openingHours[day]['open']=openClose[:5].replace("h",":")
                    openingHours[day]['close'] = openClose[-5:].replace("h", ":")





            yield POI(address=address,lat=lat,lon=lon,city=city,zipCode=zipCode,tel=tel,openingHours=openingHours,name=name)

