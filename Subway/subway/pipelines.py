# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from airflow.hooks.postgres_hook import PostgresHook
class SubwayPipeline(object):


    def __init__(self):


        self.pg_hook=PostgresHook('postgres_ds')
        self.nameTable='frederic.subwayPOI'


    def process_item(self, item, spider):

        lat=item['lat']
        lon=item['lon']
        adress=item['address']
        city=item['city']
        postalCode=item['postalCode']
        tel=item['tel']
        openingHours=item['openingHours']
        for key,value in openingHours.items():
            for key1,value1 in openingHours[key].items():
                openingHours[key][key1]=str(value1)

        print(str(openingHours))
        queryInsert= 'INSERT INTO ' + self.nameTable + ' VALUES ('+lat+','+lon+',\''+adress.replace('\'',' ')+'\',\''+city+'\','+postalCode+',\''+tel+'\',\''+str(openingHours).replace('\'','\"')+'\');'
        self.pg_hook.run(queryInsert)
        return item
    def open_spider(self,spider):

        queryDrop = 'DROP TABLE IF EXISTS '+self.nameTable
        self.pg_hook.run(queryDrop)
        queryCreate = "CREATE TABLE " + self.nameTable + "(lat double precision, lon double precision,address text,city text,postalCode int, tel text,openingHours json);"
        self.pg_hook.run(queryCreate)

    # def from_crawler(cls, crawler):
    #     pg_Hook = getattr(crawler.spider, 'pg_Hook')
    #     return self(pg_Hook)
    # @classmethod
    # def from_crawler(cls, crawler):
    #     return cls(
    #         pg_hook=crawler.settings.get('PG_HOOK'),
    #
    #     )
