# -*- coding: utf-8 -*-


dayJour= {"Lundi":"monday","Mardi":"tuesday","Mercredi":"wednesday","Jeudi":"thursday"
    ,"Vendredi":"friday","Samedi" :"saturday",
    "Simanche":"sunday"}


import scrapy
# from utils.subway.items import POI
from subway.items import POI

class spider11(scrapy.Spider):
    name = 'spider11'
    allowed_domains = ['subwayfrance.fr']
    start_urls = ['http://subwayfrance.fr/trouver-un-restaurant/recherche/48.8576375_2.380338100000017/10000000000/boulevard%20voltaire']

    def parse(self, response):
        openingHours={}
        dayJour = {"Lundi": "monday", "Mardi": "tuesday", "Mercredi": "wednesday", "Jeudi": "thursday"
            , "Vendredi": "friday", "Samedi": "saturday",
                   "Dimanche": "sunday"}

        SET_SELECTOR = 'div[class*="restaurant"]'
        for brickset in response.css(SET_SELECTOR):

            NAME_SELECTOR = 'div[class="details"] p[class="address"]  ::text'
            TEL_SELECTOR = 'div[class="details"] p[class="phone"]  ::text'
            address=brickset.css(NAME_SELECTOR).extract_first()
            cityCode=brickset.css(NAME_SELECTOR)[1].extract()
            city=cityCode[:cityCode.find(",")]
            postalCode=cityCode[-6:]
            tel=brickset.css(TEL_SELECTOR).extract_first()
            GPS_SELECTOR='::attr(data-gps)'
            gps=brickset.css(GPS_SELECTOR).extract_first()
            lat=gps[:gps.find(",")]
            lon = gps[gps.find(",")+1:]

            # OPENINGHOURS_SELECTOR='div[class="hours"] p[class="subtitle"]'
            OPENINGHOURS_SELECTOR='tr'
            openingHours={}
            for row in brickset.css(OPENINGHOURS_SELECTOR):
                # for subrow in row.xpath('tr/.'):
                #     hour=subrow.xpath('td/text()').extract_first()
                #     day=subrow.xpath('th/text()').extract_first(


                hour=row.css(' td ::text').extract_first()
                day=row.css(' th ::text').extract_first()

                open=hour[:5]
                close=hour[-7:-2]

                openingHours[dayJour[day]]={}
                openingHours[dayJour[day]]["open"]=open
                openingHours[dayJour[day]]["close"] = close





            yield POI(address=address,lat=lat,lon=lon,city=city,postalCode=postalCode,tel=tel,openingHours=openingHours)

