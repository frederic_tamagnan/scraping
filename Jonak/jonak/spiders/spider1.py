# -*- coding: utf-8 -*-


dayJour= {"Lundi":"monday","Mardi":"tuesday","Mercredi":"wednesday","Jeudi":"thursday"
    ,"Vendredi":"friday","Samedi" :"saturday",
    "Simanche":"sunday"}


import scrapy
from collections import OrderedDict


from jonak.items import POI


class spiderJonak(scrapy.Spider):
    name = 'spiderJonak'

    start_urls = ['http://www.jonak.fr/ajax_generate_xml_map.php?search=&rayon=3000km']

    def parse(self, response):
        openingHours={}



        for brickset in response.xpath('/marqueurs/marqueur'):
            if brickset.xpath('@pays').extract_first()=="France":
                lat=brickset.xpath('@lat').extract_first()
                lon=brickset.xpath('@lng').extract_first()
                name = brickset.xpath('@nom').extract_first()
                address=brickset.xpath('@adresse').extract_first()
                zipCode = brickset.xpath('@cp').extract_first()
                city=brickset.xpath('@ville').extract_first()
                tel=brickset.xpath('@telephone').extract_first()
                openingHours=self.splitOpeningHours(brickset.xpath('@horaire').extract_first())

                yield POI(address=address,lat=lat,lon=lon,city=city,zipCode=zipCode,tel=tel,openingHours=openingHours,name=name)





    @staticmethod
    def splitOpeningHours(openingHoursStr):
        dayJour = {"Lundi": "monday", "Mardi": "tuesday", "Mercredi": "wednesday", "Jeudi": "thursday", "Vendredi": "friday", "Samedi": "saturday","Dimanche": "sunday"}

        listeJour=["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
        split = openingHoursStr.split("<br/>")


        openingHours={"monday":{"open":"/","close":"/"},"tuesday": {"open": "/", "close": "/"},"wednesday": {"open": "/", "close": "/"},"thursday": {"open": "/", "close": "/"},"friday": {"open": "/", "close": "/"},"saturday": {"open": "/", "close": "/"},"sunday": {"open": "/", "close": "/"}}



        splitMinima=[]
        for piece in split:
            splitMinima = []
            splitMinima = piece.split(" : ")

            days=splitMinima[0]
            hour=splitMinima[1]

            open=hour.split(" - ")[0].replace("h",":")
            close = hour.split(" - ")[1].replace("h", ":")
            if "-" in days:
                splitDays=days.split(" - ")
                firstDay=splitDays[0]
                lastDay=splitDays[1]

                for key in listeJour[listeJour.index(firstDay):listeJour.index(lastDay)+1]:
                    openingHours[dayJour[key]]["open"]=open
                    openingHours[dayJour[key]]["close"]=close



            else:

                openingHours[dayJour[days]]["open"]=open
                openingHours[dayJour[days]]["close"]=close




        return openingHours
