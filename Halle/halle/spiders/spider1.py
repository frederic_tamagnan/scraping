# -*- coding: utf-8 -*-


dayJour= {"Lundi":"monday","Mardi":"tuesday","Mercredi":"wednesday","Jeudi":"thursday"
    ,"Vendredi":"friday","Samedi" :"saturday",
    "Simanche":"sunday"}


import scrapy
from xml.etree.ElementTree import XML, fromstring, tostring

from halle.items import POI
import unicodedata

class spiderHalle(scrapy.Spider):
    name = 'spiderHalle'

    # start_urls = ['https://www.minelli.fr/cookietest/storelocator/store/locations/country_code/FRA']

    def start_requests(self):

        cookies = {
            'tc_cj_v2': 'm_iZZZ%22**%22%27%20ZZZKOJLRSKQPPKQMZZZ%5D777%5Ecl_%5Dny%5B%5D%5D_mmZZZZZZKOJLRSKQRJKLNZZZ%5D777m_iZZZ%22**%22%27%20ZZZKOJLRSKSNLQNJZZZ%5D777%5Ecl_%5Dny%5B%5D%5D_mmZZZZZZKOJLRSLJPMRRSZZZ%5D777m_iZZZ%22**%22%27%20ZZZKOJLRSLJQMSSMZZZ%5D777%5Ecl_%5Dny%5B%5D%5D_mmZZZZZZKOJLRSLJQRNLOZZZ%5D777m_iZZZ%22**%22%27%20ZZZKOJLSJJJJQPRMZZZ%5D777%5Ecl_%5Dny%5B%5D%5D_mmZZZZZZKOJLSJJJLMKOMZZZ%5D777m_iZZZ%22**%22%27%20ZZZKOJLSQMNQSMLPZZZ%5D777%5Ecl_%5Dny%5B%5D%5D_mmZZZZZZKOJLSQNRNQLKPZZZ%5D',
            'ABTasty': 'uid%3D17080717275631185%26fst%3D1502119676527%26pst%3D1502900007786%26cst%3D1502973479454%26ns%3D4%26pvt%3D51%26pvis%3D12%26th%3D206172.282350.23.2.4.0.1502119720791.1502973567708.1',
            'eb-abtesting': '0',
            '_ga': 'GA1.2.36352150.1502119680',
            '_tli': '4075320233708375617',
            '_tlc': ':1502973495:www.lahalle.com%2Fmagasins:lahalle.com',
            '_tlv': '4.1502119680.1502900025.1502974848.51.3.12',
            '_tlp': '1319:7271030',
            'TC_OPTOUT': '0@@@1@@@ALL',
            'Tracking_Referer': 'directaccess',
            'Tracking_Date_First_Clic': '1503406847',
            'Tracking_Send_With_Ajax': 'false',
            'Tracking_Destination': 'http://www.lahalle.com/magasins/Paris',
            '_gid': 'GA1.2.2081332569.1502891082',
            'TESTCOOKIE': '1',
            'SLOT_ID': '59958E261909CF1FA3',
            'ABTastySession': 'referrer%3Dhttps%3A//www.google.fr/__landingPage%3Dhttp%3A//www.lahalle.com/magasins__referrerSent%3Dtrue',
            'ow_cookielist': '/autre/',
            'eb-abtesting-home': '2',
            'PRIVACY': '12',
            'frontend': 'hcfqtthqtcglfhborg8ufa0113',
            'last_visit': '2017%2F08%2F17+15%3A00%3A05',
            '_tls': '*.793201:793203:793202..4075320233708375617',
            '__netid': 'kfg41h2oncmgwjrltqlq7o9358',
            '__netsp': 'true',
            'tc_key': '573196',
            'RT': '',
            '_gat_vivarte': '1',
        }

        headers = {
            'Host': 'www.lahalle.com',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
            'Referer': 'http://www.lahalle.com/magasins/Paris',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest',
            'Connection': 'keep-alive',
        }

        data = [
            ('latitude', '48.856614'),
            ('longitude', '2.3522219000000177'),
            ('minLongitude', '-4.7868'),
            ('maxLongitude', '8.258'),
            ('minLatitude', '42.3254'),
            ('maxLatitude', '51.083'),
            ('zoom', '12'),
            ('storeType', ''),
        ]


        return[scrapy.FormRequest("http://www.lahalle.com/storelocator/magasins/getStoresByPosition/",cookies=cookies,formdata=data,headers=headers)]



    def parse(self, response):

        openingHours={}
        dayJour = {"Lundi": "monday", "Mardi": "tuesday", "Mercredi": "wednesday", "Jeudi": "thursday"
            , "Vendredi": "friday", "Samedi": "saturday",
                   "Dimanche": "sunday"}


        for brickset in response.xpath('//*[contains(@class,"infoMarkerStorelocator")]'):
            address=brickset.xpath('//*[contains(@class,"adressInfoMarkerStorelocator")]/text()').extract_first()

            zipCode=brickset.xpath('//*[contains(@class,"adressInfoMarkerStorelocator")]/br/text()').extract_first()
            tel=brickset.xpath('//*[contains(@class,"adressInfoMarkerStorelocator")]/br/text()').extract()
            lat=''
            lon=''

            tel=''
            openingHours=''
            name=brickset.xpath('p/strong/text()').extract_first()
            city=''




            yield POI(address=address,lat=lat,lon=lon,city=city,zipCode=zipCode,tel=tel,openingHours=openingHours,name=name)

